<head>
<?php JHtml::_('behavior.framework', true);
if(class_exists('JHtmlJquery')) JHtml::_('jquery.framework');?>
<jdoc:include type="head" />
<link rel="stylesheet" href="<?php echo $template_baseurl;?>/css/bootstrap/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $template_baseurl;?>/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $template_baseurl;?>/css/styles/<?php echo $TemplateStyle;?>.css" type="text/css" />
<?php if($responsive) 
$vtOutout .= '<meta name="viewport" content="width=device-width, initial-scale=1.0">'.
     '<link rel="stylesheet" href="'.$template_baseurl.'/css/responsive.css" type="text/css" />';
else $vtOutout .= '<style type="text/css">.container{width: 970px !important;}</style>';
if($webfont == 1){
$vtOutout .= '<script type="text/javascript">
      WebFontConfig = {
        google: { families: ["'.$googlefont.'"] }
      };
      (function() {
        var wf = document.createElement("script");
        wf.src = ("https:" == document.location.protocol ? "https" : "http") +
            "://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js";
        wf.type = "text/javascript";
        wf.async = "true";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(wf, s);
      })();
    </script>';
}
if($copyright == 1){
$vtcopyright = '<div style="text-align:center;padding:5px;"><a href="http://vtem.net" target="_blank"><img class="vtem_copyright_logo" src="'.$template_baseurl.'/vtemtools/widgets/images/vtem-logo.png"/></a></div>';
}else{$vtcopyright = '';}
$vtOutout .= '<style type="text/css">body#vtem{font-family:'.$fontfamily.' !important; font-size:'.$fontsize.' !important;}'.(($googlefont != "" && $webfont == 1) ? $googlefontelements.'{font-family:"'.$googlefont1[0].'", sans-serif;}' : '').'</style>';
?>
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>