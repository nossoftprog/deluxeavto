<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
$navoption = '';
$navigation = $params->get('slideshow-nav', 'dots');
$navAlign = $params->get('slideshow-nav-align', 'left');
$progressbar = $params->get('slideshow-progressbar', 0);
$slideshoweffect = $params->get('slideshow-effect', 'random');
switch ($navigation) {
    case "dots":
        $navoption = 'dots: true, numbers: false, preview: false, thumbs: false';
        break;
    case "dotspreview":
        $navoption = 'dots: true, numbers: false, preview: true, thumbs: false';
        break;
    case "numbers":
        $navoption = 'dots: false, numbers: true, preview: false, thumbs: false';
        break;
	case "thumbs":
        $navoption = 'dots: false, numbers: false, preview: false, thumbs: true';
        break;
	case "none":
        $navoption = 'dots: false, numbers: false, preview: false, thumbs: false';
        break;
}
?>
<script type="text/javascript" src="<?php echo JURI::root(true).'/modules/'.$module->module.'/styles/jquery-ui-1.9.2.custom.min.js';?>"></script>
<script type="text/javascript" src="<?php echo JURI::root(true).'/modules/'.$module->module.'/styles/jquery.skitter.js';?>"></script>
<script type="text/javascript">
var vtemengine = jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery('#vtemskitter<?php echo $module_id;?>').skitter({
		 width:  '<?php echo $width;?>',
		 height: '<?php echo $height == 'auto' ? '50%' : $height;?>',
		 animation: '<?php echo $slideshoweffect;?>', 
		 navigation: <?php echo $prevnext;?>,
		 stop_over: <?php echo $pauseonhover;?>,
		 numbers_align: '<?php echo $navAlign;?>',
		 auto_play: <?php echo $auto_play;?>,
		 interval: <?php echo $auto_play ? $autoplay_delay : 9000000;?>, 
		 progressbar: <?php echo $progressbar;?>,
		 label: 1,
		 width_label: '60%',
		 <?php echo $navoption;?>
	}); 
});
</script>
<?php if($beforetext != '') echo '<div class="vtem-before-text">'.$beforetext.'</div>';?>
<div id="vtemskitter<?php echo $module_id;?>" class="vtem_skitter_<?php echo $navigation;?> vtem_skitter clearfix skitter<?php echo $params->get('moduleclass_sfx');?>">
        <dl id="vtem-<?php echo $module_id;?>-skitter" class="skitter-data">
            <?php
			if($content_source == "images"){
				foreach($images as $key => $img):
				      $vttitles = explode(";",$params->get('imagetitle'));
					  $vttitle = (isset($vttitles[$key])) ? $vttitles[$key] : '';
					  $vtcontents = explode(";",$params->get('imagecontent'));
					  $vtcontent = (isset($vtcontents[$key])) ? $vtcontents[$key] : '';
					  $vtlinks = explode(";",$params->get('urls'));
					  $vtlink = (isset($vtlinks[$key])) ? $vtlinks[$key] : '';
						  echo '<dd id="vtem'.$key.'">';
						          if($linkedimage == 1){
						           echo '<a href="'.trim($vtlink).'" target="'.$linktarget.'"><img class="vt_skitter_thumb" src="'.JURI::root().$imagePath.$img.'" alt="VTEM skitter" /></a>';
								   }else{
								   echo '<img class="vt_skitter_thumb" src="'.$imagePath.$img.'" alt="VTEM skitter" />';
								   }
								   if($showcaption == 1){
								       echo '<div class="label_text">
											 <h4 class="vtem_skitter_title">'.trim($vttitle).'</h4>
											 <div>'.trim($vtcontent).'</div>
										 </div>';
								   }
						  echo '</dd>';
				endforeach;
		}else{
                for($i=0; $i<count($list); $i++){
                    if($list[$i]->introtext != NULL){
                        echo "<dd id='vtem".$i."' >".$list[$i]->introtext."</dd>";
                    }
                }
		}
                ?>
        </dl>
</div>
<?php if($aftertext != '') echo '<div class="vtem-after-text">'.$aftertext.'</div>';?>