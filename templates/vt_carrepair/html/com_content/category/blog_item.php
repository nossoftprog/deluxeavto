<?php

defined('_JEXEC') or die;?>

<?php

// Create a shortcut for params.

$params = $this->item->params;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

$canEdit = $this->item->params->get('access-edit');

JHtml::_('behavior.framework');



$output ='';

if ($this->item->state == 0) : 

	$output .= '<span class="label label-warning">'.JText::_('JUNPUBLISHED').'</span>';

endif; 

$jversion = new JVersion;

//CHECK JOOMLA VERSION

if (version_compare($jversion->getShortVersion(), '3.0.0', '<')){ //JOOMLA 2.5.x

	if ($params->get('show_title')) :

		$output .='<div class="page-header"><h1>';

		if ($params->get('link_titles') && $params->get('access-view')) :

			$output .= '<a href="'.JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language)).'">'.$this->escape($this->item->title).'</a>';

		else :

			$output .= $this->escape($this->item->title);

		endif;

		$output .='</h1></div>';

	endif;

	if ($params->get('show_print_icon') || $params->get('show_email_icon') || $canEdit) :

		$output .='<div class="btn-group pull-right actions">

			<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"><span class="glyphicon glyphicon-cog"></span></button>

			<ul class="dropdown-menu" role="menu">';

		if ($params->get('show_print_icon')) :

			$output .='<li class="print-icon">'.JHtml::_('icon.print_popup', $this->item, $params).'</li>';

		endif;

		if ($params->get('show_email_icon')) :

			$output .='<li class="email-icon">'.JHtml::_('icon.email', $this->item, $params).'</li>';

		endif;

		if ($canEdit) :

			$output .='<li class="edit-icon">'.JHtml::_('icon.edit', $this->item, $params).'</li>';

		endif;

		$output .='</ul></div>';

	endif;

	if (!$params->get('show_intro')) :

		$output .= $this->item->event->afterDisplayTitle;

	endif;

	$output .= $this->item->event->beforeDisplayContent;

	if (($params->get('show_author')) or ($params->get('show_category')) or ($params->get('show_create_date')) or ($params->get('show_modify_date')) or ($params->get('show_publish_date')) or ($params->get('show_parent_category')) or ($params->get('show_hits'))) :

		$output .='<dl class="article-info"><dt class="article-info-term">'.JText::_('COM_CONTENT_ARTICLE_INFO').'</dt>';

	endif;

		if ($params->get('show_parent_category') && $this->item->parent_id != 1) :

			$output .='<dd class="parent-category-name">';

				$title = $this->escape($this->item->parent_title);

				$url = '<a href="' . JRoute::_(ContentHelperRoute::getCategoryRoute($this->item->parent_id)) . '">' . $title . '</a>';

				if ($params->get('link_parent_category')) :

					$output .= JText::sprintf('COM_CONTENT_PARENT', $url);

				else:

					$output .= JText::sprintf('COM_CONTENT_PARENT', $title);

				endif;

			$output .='</dd>';

		endif;

		if ($params->get('show_category')) :

			$output .='<dd class="category-name">';

				$title = $this->escape($this->item->category_title);

				$url = '<a href="' . JRoute::_(ContentHelperRoute::getCategoryRoute($this->item->catid)) . '">' . $title . '</a>';

				if ($params->get('link_category')) :

					$output .=JText::sprintf('COM_CONTENT_CATEGORY', $url);

				else:

					$output .=JText::sprintf('COM_CONTENT_CATEGORY', $title);

				endif;

			$output .='</dd>';

		endif;

		if ($params->get('show_create_date')) :

			$output .='<dd class="create">'.JText::sprintf('COM_CONTENT_CREATED_DATE_ON', JHtml::_('date', $this->item->created, JText::_('DATE_FORMAT_LC2'))).'</dd>';

		endif;

		if ($params->get('show_modify_date')) :

			$output .='<dd class="modified">'.JText::sprintf('COM_CONTENT_LAST_UPDATED', JHtml::_('date', $this->item->modified, JText::_('DATE_FORMAT_LC2'))).'</dd>';

		endif;

		if ($params->get('show_publish_date')) :

			$output .='<dd class="published">'.JText::sprintf('COM_CONTENT_PUBLISHED_DATE_ON', JHtml::_('date', $this->item->publish_up, JText::_('DATE_FORMAT_LC2'))).'</dd>';

		endif;

		if ($params->get('show_author') && !empty($this->item->author )) :

			$output .='<dd class="createdby">';

				$author =  $this->item->author;

				$author = ($this->item->created_by_alias ? $this->item->created_by_alias : $author);

				if (!empty($this->item->contactid ) &&  $params->get('link_author') == true):

					$output .= JText::sprintf('COM_CONTENT_WRITTEN_BY', JHtml::_('link', JRoute::_('index.php?option=com_contact&view=contact&id='.$this->item->contactid), $author));

				else:

					$output .= JText::sprintf('COM_CONTENT_WRITTEN_BY', $author);

				endif;

			$output .='</dd>';

		endif;

		if ($params->get('show_hits')) :

			$output .='<dd class="hits">'.JText::sprintf('COM_CONTENT_ARTICLE_HITS', $this->item->hits).'</dd>';

		endif;

	if (($params->get('show_author')) or ($params->get('show_category')) or ($params->get('show_create_date')) or ($params->get('show_modify_date')) or ($params->get('show_publish_date')) or ($params->get('show_parent_category')) or ($params->get('show_hits'))) :

		$output .='</dl>';

	endif;

	if (isset($images->image_intro) and !empty($images->image_intro)) :

		$imgfloat = (empty($images->float_intro)) ? $params->get('float_intro') : $images->float_intro;

		$output .='<div class="img-intro-'.htmlspecialchars($imgfloat).'"><img src="'.htmlspecialchars($images->image_intro).'" alt="'.htmlspecialchars($images->image_intro_alt).'" /></div>';

	endif;

	$output .= $this->item->introtext;

	

}else{ //JOOMLA 3.x

	

	$output .= JLayoutHelper::render('joomla.content.blog_style_default_item_title', $this->item).

		 JLayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item, 'print' => false));

	

		$useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')

		|| $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author') );

	

		if ($useDefList) : 

			$output .= JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'above'));

		endif;

	$output .= JLayoutHelper::render('joomla.content.intro_image', $this->item); 



	if (!$params->get('show_intro')) : 

		$output .= $this->item->event->afterDisplayTitle;

	endif;

	$output .= $this->item->event->beforeDisplayContent; 

	$output .= $this->item->introtext;



	if ($useDefList) : 

		$output .= JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'below'));

	endif;

	

}



    if ($params->get('show_readmore') && $this->item->readmore) :

		if ($params->get('access-view')) :

			$link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));

		else :

			$menu = JFactory::getApplication()->getMenu();

			$active = $menu->getActive();

			$itemId = $active->id;

			$link1 = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);

			$returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));

			$link = new JUri($link1);

			$link->setVar('return', base64_encode($returnURL));

		endif; 

		$output .= '<p class="readmore"><a class="btn btn-primary" href="'.$link.'">';



		if (!$params->get('access-view')) :

			$output .= JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');

		elseif ($readmore = $this->item->alternative_readmore) :

			$output .= $readmore;

			if ($params->get('show_readmore_title', 0) != 0) :

				$output .= JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));

			endif;

		elseif ($params->get('show_readmore_title', 0) == 0) :

			$output .= JText::sprintf('COM_CONTENT_READ_MORE_TITLE');

		else :

			$output .= JText::_('COM_CONTENT_READ_MORE').

					   JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));

		endif;

		$output .= '<i class="fa fa-arrow-right"> &nbsp; </i>';

		$output .= '</a></p>';

	endif;

	$output .= $this->item->event->afterDisplayContent; 

echo $output;

?>

