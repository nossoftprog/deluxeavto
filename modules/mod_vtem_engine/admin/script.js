/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/


var vtemEngine = jQuery.noConflict();
jQuery(document).ready( function(){  
	 jQuery('.vt-para, .vt-label').closest('li, .control-group').addClass("vt-hide");
	 
	 jQuery('.vt-para-'+jQuery('#jform_params_module_type').children(':selected').val()).closest('li,.control-group').show();
	 jQuery('#jform_params_module_type').change(function() {
			jQuery('.vt-hide').closest('li,.control-group').hide();
		    jQuery('.vt-para-'+jQuery(this).val()).closest('li,.control-group').show();
	});	
	 
	    
    //Joomla, k2 and modules  Accordion hide and show effect depend on content source.
    function showJoomla(){
        jQuery('.vt_joomla').addClass('active').slideDown();
        jQuery('.vt_k2').removeClass('active').hide();
        jQuery('.vt_mods').removeClass('active').hide();
		jQuery('.vt_images').addClass('active').hide();
    }
    function showK2(){
        jQuery('.vt_joomla').removeClass('active').hide();
        jQuery('.vt_mods').removeClass('active').hide();
        jQuery('.vt_k2').addClass('active').slideDown();
		jQuery('.vt_images').addClass('active').hide();
    }
    function showMods(){
        jQuery('.vt_joomla').removeClass('active').hide();
        jQuery('.vt_k2').removeClass('active').hide();
        jQuery('.vt_mods').addClass('active').slideDown();
		jQuery('.vt_images').addClass('active').hide();
    }
	function showImages(){
        jQuery('.vt_joomla').removeClass('active').hide();
        jQuery('.vt_k2').removeClass('active').hide();
        jQuery('.vt_mods').addClass('active').hide();
		jQuery('.vt_images').addClass('active').slideDown();
    }
    
    //determine which settings is enable in content source and show related container
    switch(jQuery('#jform_params_content_source').val()){
        case 'joomla':
            showJoomla();
            break;
        case 'k2':
            showK2();
            break;
        case 'mods':
            showMods();
            break;
		case 'images':
            showImages();
            break;
    }
    
    //change accordion realtime 
    jQuery('#jform_params_content_source').change(function(){
        switch(jQuery('#jform_params_content_source').val()){
        case 'joomla':
            showJoomla();
            break;
        case 'k2':
            showK2();
            break;
        case 'mods':
            showMods();
            break;
		case 'images':
            showImages();
            break;
    }
    });
});