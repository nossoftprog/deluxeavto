<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
// no direct access
defined('_JEXEC') or die('Restricted accessd');
if(!defined('DS')){define('DS',DIRECTORY_SEPARATOR);}
$module_id = 'tabsid'.$module->id;
$module_type = $params->get('module_type' , 'slideshow');
$content_source = $params->get('content_source', 'joomla');
$width = $params->get('width' , '100%');
$height = $params->get('height', 'auto');
$tabs = $params->get('countItems', 3);
$tabs_position = $params->get('tabs_position','top');
$mouseEvent = $params->get('mouseEvent', 'click');
$mod_title_custom = $params->get('mod_title_custom', '');
$modstyle = $params->get('modstyle', 'style1');
$transition_type = $params->get('transition_type', 'fade');
$auto_play = $params->get('auto_play', 0);
$autoplay_delay = $params->get('autoplay_delay', 4000);
$pauseonhover = $params->get('pauseonhover', 1);
$prevnext = $params->get('prevnext', 0);
$linkedimage = $params->get('linkedimage', 0);
$showcaption = $params->get('showcaption', 0);
$linktarget = $params->get('linktarget', '_parent');
$jquery = $params->get('jquery', 1);
$modulecss = $params->get('modulecss', 1);
$moduleclass_sfx = $params->get('moduleclass_sfx');
$beforetext = $params->get('beforetext', '');
$aftertext = $params->get('aftertext', '');

require_once (dirname(__FILE__).DS.'helper.php');
$imagePath 	= modVtemengineHelper::cleanDir($params->get( 'imagePath', 'images/sampledata/fruitshop' ));
$sortCriteria = $params->get( 'sortCriteria', 0);
$sortOrder = $params->get( 'sortOrder', 'asc');
$sortOrderManual = $params->get( 'sortOrderManual', '');

if($content_source == "images"){
	if (trim($sortOrderManual) != "")
		$images = explode(",", $sortOrderManual);
	else
		$images = modVtemengineHelper::imageList($imagePath, $sortCriteria, $sortOrder);
}else{	
		$list = modVtemengineHelper::getLists($params);
		$tabs_title = modVtemengineHelper::generateTabs($tabs,$list,$params);
}

$doc = JFactory::getDocument();
$jversion = new JVersion;
if($jquery ==1){
	if (version_compare($jversion->getShortVersion(), '3.0.0', '<'))
		$doc->addScript(JURI::root(true).'/modules/'.$module->module.'/styles/jquery-1.7.2.min.js');
}elseif($jquery ==2){
   $doc->addScript(JURI::root(true).'/modules/'.$module->module.'/styles/jquery-1.7.2.min.js');
}
if($module_type == 'slideshow' && $modulecss)
	$doc->addStyleSheet(JURI::root(true).'/modules/'.$module->module.'/styles/slideshow.css');
if($module_type == 'tabs' && $modulecss)
	$doc->addStyleSheet(JURI::root(true).'/modules/'.$module->module.'/styles/tabs.css');
if($module_type == 'scroller' && $modulecss)
	$doc->addStyleSheet(JURI::root(true).'/modules/'.$module->module.'/styles/scroller.css');
if($module_type == 'accordion' && $modulecss)
	$doc->addStyleSheet(JURI::root(true).'/modules/'.$module->module.'/styles/accordion.css');
if($module_type == 'lightbox' && $modulecss)
	$doc->addStyleSheet(JURI::root(true).'/modules/'.$module->module.'/styles/lightbox.css');

require(JModuleHelper::getLayoutPath($module->module, $module_type));