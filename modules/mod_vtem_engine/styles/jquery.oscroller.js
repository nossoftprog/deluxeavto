/*
 * jQuery oScroller v1.0
 * Copyright (c) 2012 OpenAddon.com/vtem.net
 * Requires: jQuery v1.4.0 or later
 * Dual licensed under the MIT and GPL licenses:
 * 	http://www.opensource.org/licenses/mit-license.php
 * 	http://www.gnu.org/licenses/gpl.html
 *
 */
(function($){
	"use strict";
	$.fn.extend({
		oScroller: function(options) {
			var defaults = { //default values for plugin options
				width: '100%',
				height: 'auto',
				effect: 'slide', //fade, slide
				autoplay: true,
				interval: 5000,
				duration: 500,
				hoverpause: false,
				pager: true,
				nextPrev: true,
				keynav: true,
				css3Animation: ''
			}
			var options =  $.extend(defaults, options);
 
			return this.each(function() {
				var opts = options,
					obj = $(this),
					slides = $('.slides > li', obj),
					pager = $('.pager > li', obj),
					current = 0,
					next = current+1,
					objPlay;
				
				obj.css({'position':'relative', 'overflow':'hidden'}).children('.slides').css({'position':'relative', 'overflow':'hidden', 'width':opts.width, 'height': opts.height});
				slides.width(opts.width).hide().eq(current).fadeIn(opts.duration).addClass('active').find('.animated').addClass(opts.css3Animation);
				//build pager if it doesn't already exist and if enabled
				if(pager.length) {
					pager.eq(current).addClass('active');
				} else if(opts.pager){
					obj.append('<ul class="pager"></ul>');
					slides.each(function(index) {
						$('.pager', obj).append('<li><a href="#"><span>'+index+'</span></a></li>')
					});
					pager = $('.pager li', obj);
					pager.eq(current).addClass('active');
				}
				//rotate to selected slide on pager click
				if(pager){
					$('a', pager).click(function() {
						clearTimeout(objPlay);//stop the timer
						next = $(this).parent().index();//set the slide index based on pager index
						if(!$(this).parent().hasClass('active'))
						rotate(next > current ? 'next': 'prev');//rotate the slides
						return false;
					});
				}
				//primary function to change slides
				var rotate = function($dir){
					//slides.eq(current).hide().removeClass('active');
					switch(opts.effect){
						default:
						case 'fade':
						  slides.eq(current).hide().removeClass('active').find('.animated').removeClass(opts.css3Animation);
						  slides.eq(next).fadeIn(opts.duration);
						  break;
						case 'blind':
						  slides.eq(current).hide().removeClass('active').find('.animated').removeClass(opts.css3Animation);
						  slides.eq(next).css('left', ($dir === 'next' ? '100%': '-100%')).show().parent().height(opts.height=='auto' ? slides.eq(next).outerHeight() : opts.height)
						  				.end().animate({'left': 0}, opts.duration);
						  break;
						case 'slide':
						  slides.eq(current).animate({'left': ($dir === 'next' ? '-100%': '100%')}, opts.duration).removeClass('active').find('.animated').removeClass(opts.css3Animation);
						  slides.eq(next).css('left', ($dir === 'next' ? '100%': '-100%')).show().parent().height(opts.height=='auto' ? slides.eq(next).outerHeight() : opts.height)
						  				.end().animate({'left': 0}, opts.duration);
						  break;
					}
					slides.eq(next).addClass('active').queue(function(){
							if(opts.autoplay) rotateTimer();
							$(this).dequeue();
					}).find('.animated').addClass(opts.css3Animation);
					
					if(pager){ //update pager to reflect slide change
						pager.eq(current).removeClass('active')
							.end().eq(next).addClass('active');
					}
					
 					current = next; //update current and next vars to reflect slide change
					next = current >= slides.length-1 ? 0 : current+1; //set next as first slide if current is the last
				};
				
				if(opts.autoplay){
					var rotateTimer = function(){ //create a timer to control slide rotation interval
						objPlay = setTimeout(function(){
							rotate('next');
						}, opts.interval);
					};
					rotateTimer(); //start the timer for the first time
				}
				if(opts.hoverpause && opts.autoplay){ //pause the slider on hover
					slides.hover(function(){
						clearTimeout(objPlay); //stop the timer in mousein
					}, function(){
						rotateTimer(); //start the timer on mouseout
					});
				}
				
				if (opts.nextPrev) {
					obj.append('<span class="oArrow prev"><i class="fa fa-arrow-circle-o-left"></i></span><span class="oArrow next"><i class="fa fa-arrow-circle-o-right"></i></span>');
					$('.oArrow', obj).click(function() {
						slides.clearQueue();
						if ($(this).hasClass('next')) {
							clearTimeout(objPlay);
							rotate('next');
						}
						if ($(this).hasClass('prev')) {
							clearTimeout(objPlay);
							if (current < 1)
								next = slides.length - 1;
							else
								next = current - 1;
							rotate('prev');
						}
					});
				}
				
				$(window).resize(function(){ //bind setsize function to window resize event
					if(opts.height=='auto'){
						obj.height(slides.eq(current).outerHeight()).children('.slides').height(slides.eq(current).outerHeight());
					}
				});
				
				//Add keyboard navigation
				if(opts.keynav){
					$(document).keyup(function(e){
						switch (e.which) {
							case 39: case 32: //right arrow & space
								clearTimeout(objPlay);
								rotate('next');
								break;
							case 37: // left arrow
								clearTimeout(objPlay);
								next = current - 1;
								rotate('prev');
								break;
						}
					});
				}
			});
		}
	});
})(jQuery);
