/*
 * jQuery oTab v1.0
 * Copyright (c) 2012 OpenAddon.com/vtem.net
 * Requires: jQuery v1.4.0 or later
 * Dual licensed under the MIT and GPL licenses:
 * 	http://www.opensource.org/licenses/mit-license.php
 * 	http://www.gnu.org/licenses/gpl.html
 *
 */

;(function($){
	"use strict";
	$.fn.extend({
		oTab: function(options) {
			var defaults = { //default values for plugin options
				width: '100%',
				height: 'auto',
			    autoplay: false,
				duration: 5000,
				speed: 500,
				effect: 'fade', //fade, scrollLeft, scrollRight, drop, bounce
				hoverpause: false,
				tabNav: true,
				tabEvent: 'click', //click, mouseover, mouseenter
				nextPrev: true,
				tabActiveClass: 'tabActive',
				panelActiveClass: 'panelActive',
				keynav: true
			}
			var options =  $.extend(defaults, options);
			return this.each(function() {
				var opts = options,
				    obj = $(this),
					slides = $('.oPanels > li, .oPanels > div', obj),
					pager = $('.oTabs > li', obj),
					current = 0,
					next = current+1,
					setHeight = opts.height,
					objPlay;
				slides.hide().eq(current).show().addClass(opts.panelActiveClass);
				if(opts.width.indexOf('%')!=-1 || opts.width=='auto')//SET WIDTH FOR MODULE
					opts.width = opts.width;
				else
					opts.width = parseFloat(opts.width);
				if(opts.height.indexOf('%')!=-1)//SET HEIGHT FOR MODULE
					opts.height = parseFloat(Math.round(obj.parent(':visible').width() / (100/parseFloat(opts.height))));
				else if (opts.height=='auto')
					opts.height = opts.height;
				else
					opts.height = parseFloat(opts.height);
				obj.wrap('<div class="oTabWrapper" />').addClass('oTabContainer').css({'width':opts.width, 'height':opts.height, 'overflow':'hidden'})
				   .find('.oPanels').css({'width':opts.width, 'height':(setHeight=='auto' ? 'auto' : (obj.height() - obj.find('.oTabs').outerHeight()))});
					
 				if(pager.length && opts.tabNav){ //rotate to selected slide on pager click
					pager.eq(current).addClass(opts.tabActiveClass);
					$('a', pager).bind(opts.tabEvent, function(){
						clearTimeout(objPlay);
						next = $(this).parent().index();
						rotate();
						return false;
					});
				}

				var rotate = function(){ //primary function to change slides
					slides.eq(current).hide().removeClass(opts.panelActiveClass);
					switch(opts.effect){
						default:
						case 'fade':
						  slides.eq(next).fadeIn(opts.speed);
						  break;
						case 'drop':
						  slides.eq(next).slideDown(opts.speed);
						  break;
						case 'scrollLeft':
						  slides.eq(next).css({'position': 'absolute', 'top':0, 'left': '100%', 'width': '100%'}).show()
							  .end().parent().height(setHeight=='auto' ? slides.eq(next).outerHeight() : opts.height)
						  	  .end().animate({'left': 0}, opts.speed);
						  break;
						case 'scrollRight':
						  slides.eq(next).css({'position': 'absolute', 'top':0, 'left': '-100%', 'width': '100%'}).show()
							  .end().parent().height(setHeight=='auto' ? slides.eq(next).outerHeight() : opts.height)
						  	  .end().animate({'left': 0}, opts.speed);
						  break;
						case 'bounce':
						  slides.parent().height(setHeight=='auto' ? slides.eq(next).outerHeight() : opts.height)
						  	  .end().eq(next).css({'position': 'absolute', 'top': '100%', 'left': 0, 'width': '100%'}).show()
						  	  .end().animate({'top': 0}, opts.speed);
						  break;
					}
					slides.eq(next).addClass(opts.panelActiveClass).queue(function(){
						if(opts.autoplay) rotateTimer();
						$(this).dequeue()
					});
					if(pager.length && opts.tabNav){ //update pager to reflect slide change
						pager.eq(current).removeClass(opts.tabActiveClass)
							.end().eq(next).addClass(opts.tabActiveClass);
					}
					current = next;
					next = current >= slides.length-1 ? 0 : current+1;
				};
				
				if(opts.autoplay){
					var rotateTimer = function(){ //create a timer to control slide rotation interval
						 objPlay = setTimeout(function(){ 
						 	rotate();
						}, opts.duration);
					};
					rotateTimer(); //start the timer for the first time
				}
				
				if(opts.hoverpause && opts.autoplay){ //pause the slider on hover
					slides.hover(function(){
						clearTimeout(objPlay);
					}, function(){
						rotateTimer();
					});
				}
				
				if (opts.nextPrev) {
					obj.append('<span class="oArrow prev">Prev</span><span class="oArrow next">Next</span>');
					$('.oArrow', obj).click(function() {
						if ($(this).hasClass('next')) {
							clearTimeout(objPlay);
							rotate();
						}
						if ($(this).hasClass('prev')) {
							clearTimeout(objPlay);
							if (current < 1)
								next = slides.length - 1;
							else
								next = current - 1;
							rotate();
						}
					});
				}
				
				$(window).resize(function(){ //bind setsize function to window resize event
					if(setHeight.indexOf('%')!=-1){
						var startH = Math.round(obj.parent(':visible').width() / (100/parseFloat(setHeight)));
						obj.height(startH).find('.oPanels').height(startH);
					}
				});

				if(opts.keynav){ //Add keyboard navigation
					$(document).keyup(function(e){
						switch (e.which) {
							case 39: case 32: //right arrow & space
								clearTimeout(objPlay);
								rotate();
								break;
							case 37: // left arrow
								clearTimeout(objPlay);
								if (current < 1)
									next = slides.length - 1;
								else
									next = current - 1;
								rotate();
								break;
						}
					});
				}
			});
		}
	});
})(jQuery);