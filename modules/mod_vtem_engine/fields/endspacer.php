<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/

// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();

jimport('joomla.html.html');
jimport('joomla.form.formfield');
class JFormFieldEndspacer extends JFormField
{
    protected $type = 'endspacer';
    protected function getInput()
    {
		$jversion = new JVersion;
        if ($this->name) {
            $class = $this->element['class']; 
			if (version_compare($jversion->getShortVersion(), '3.0.0', '<')){             		
            	$paneCloses = '</fieldset><li style="display:none;">';
			}else{
				$paneCloses = '</div>';
			}
            return $paneCloses;
        } else {
            return '<hr />';
        }
    }
}
