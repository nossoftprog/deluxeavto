<?php
// no direct access
defined('_JEXEC') or die ('Restricted access');
jimport('joomla.html.html');
jimport('joomla.form.formfield');
class JFormFieldK2Categories extends JFormField
{
	var $_name = 'k2categories';

	public function getInput()
	{	
		$db    = JFactory::getDBO();
		// Initialize some field attributes.
		$attr = '';
		$attr .= $this->element['class'] ? ' class="' . (string)$this->element['class'] . '"' : '';
		if ((string)$this->element['readonly'] == 'true' || (string)$this->element['disabled'] == 'true')
			$attr .= ' disabled="disabled"';
		$attr .= $this->element['size'] ? ' size="' . (int)$this->element['size'] . '"' : '';
		$attr .= $this->element['multiple'] ? ' multiple="multiple"' : '';
		$attr .= $this->element['onchange'] ? ' onchange="' . (string)$this->element['onchange'] . '"' : '';

		$mitems = array();
		if ($this->available()) {
			$query = 'SELECT m.* FROM #__k2_categories m WHERE published=1 AND trash = 0 ORDER BY parent, ordering';
			$db->setQuery($query);
			$mitems   = $db->loadObjectList();
			$children = array();
			if ($mitems) {
				foreach ($mitems as $v) {
					$v->title     = $v->name;
					$v->parent_id = $v->parent;
					$pt           = $v->parent;
					$list         = @$children[$pt] ? $children[$pt] : array();
					array_push($list, $v);
					$children[$pt] = $list;
				}
			}
			$list   = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);
			$mitems = array();
			foreach ($list as $item) {
				$item->treename = JString::str_ireplace('&#160;', '- ', $item->treename);
				$mitems[]       = JHtml::_('select.option', $item->id, '   ' . $item->treename);
			}
		}
		return JHtml::_('select.genericlist', $mitems, $this->name, trim($attr), 'value', 'text', $this->value);
	}

	function available()
	{
		$jversion = new JVersion;
		if (version_compare($jversion->getShortVersion(), '3.0.0', '<')){ 
			return true;
		}else{
			$db    = JFactory::getDBO();
			$query = 'select count(*) from #__extensions where element = ' . $db->Quote('com_k2') . ' AND enabled=1';
			$db->setQuery($query);
			$count = (int)$db->loadResult();
			if ($count > 0) {
				return true;
			}
			return false;
		}
	}
}
