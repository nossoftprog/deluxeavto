<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
$title_type = $params->get('mod_title_type');
$accordionOneopen = $params->get('accordionOneopen', 1);
?>
<script type="text/javascript" src="<?php echo JURI::root(true).'/modules/'.$module->module.'/styles/jquery.msAccordion.js';?>"></script>
<script type="text/javascript">
var vtemaccordion = jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery("#vtem<?php echo $module_id;?>-accordion").msAccordion({
		width:  '<?php echo $width;?>',
		height: '<?php echo $height;?>',
		vertical: 1,
		defaultid:0,
		autodelay:<?php echo $auto_play ? $autoplay_delay : 0;?>,
		event:"<?php echo $mouseEvent;?>",
		onemustopen: <?php echo $accordionOneopen;?>
	}); 
});
</script>
<div id="<?php echo $module_id;?>" class="vtem-accordion-wrapper clearfix vtemaccordion-custom module<?php echo $moduleclass_sfx; ?>">
	<?php if($beforetext != '') echo '<div class="vtem-before-text">'.$beforetext.'</div>';?>
	<div id="vtem<?php echo $module_id;?>-accordion" class="vtem-accordion clearfix accordion-1">
            <?php
			if($content_source == "images"){
				foreach($images as $key => $img):
				      $vttitles = explode(";",$params->get('imagetitle'));
					  $vttitle = (isset($vttitles[$key])) ? $vttitles[$key] : '';
					  $vtcontents = explode(";",$params->get('imagecontent'));
					  $vtcontent = (isset($vtcontents[$key])) ? $vtcontents[$key] : '';
					  $vtlinks = explode(";",$params->get('urls'));
					  $vtlink = (isset($vtlinks[$key])) ? $vtlinks[$key] : '';
					  if($title_type == 'custom'){
						$titles = explode(";",$params->get('mod_title_custom'));
						$title = (isset($titles[$key])) ? $titles[$key] : '';
					  }else $title = 'Accordion'.($key+1);
						  echo '<div class="set"><div class="title"><div class="toggler"><span class="vt_number">'.($key+1).'</span>
<span class="vt_accordion_title">'.$title.'</span></div></div><div class="content"><div class="accordion-content">';
						          if($linkedimage == 1){
						           echo '<a href="'.trim($vtlink).'" target="'.$linktarget.'"><img class="vt_skitter_thumb" src="'.JURI::root().$imagePath.$img.'" alt="VTEM skitter" /></a>';
								   }else{
								   echo '<img class="vt_skitter_thumb" src="'.$imagePath.$img.'" alt="VTEM skitter" />';
								   }
								   if($showcaption == 1){
								       echo '<div class="label_text">
											 <h4 class="vtem_skitter_title">'.trim($vttitle).'</h4>
											 <div>'.trim($vtcontent).'</div>
										 </div>';
								   }
						  echo "</div></div></div>";
				endforeach;
		}else{
                for($i=0; $i<count($list); $i++){
				    if($title_type == 'custom'){
						$titles = explode(";",$params->get('mod_title_custom'));
						$title = (isset($titles[$i])) ? $titles[$i] : '';
					}else $title = $list[$i]->title;
				    echo '<div class="set"><div class="title"><div class="toggler"><span class="vt_number">'.($i+1).'</span>
<span class="vt_accordion_title">'.$title.'</span></div></div>';
                    if($list[$i]->introtext != NULL){
                        echo "<div class='content'><div class='accordion-content'><div class='vt_accordion_main'><table class='vt_accordion_main_item'><tr><td>\n";
                            echo $list[$i]->introtext;?>
                        <?php echo "</td></tr></table></div></div></div>\n";
                    }
					echo '</div>';
                }
		}
       ?>       
	</div>
	<?php if($aftertext != '') echo '<div class="vtem-after-text">'.$aftertext.'</div>';?>
</div>
